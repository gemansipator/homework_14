import { Link } from "react-router-dom"

function IndexPage() {
    return (
        <>
            <h1>Главная страница</h1>
            <Link to={'/products'}>Страница продуктов</Link>
        </>
    )
}

export default IndexPage;

