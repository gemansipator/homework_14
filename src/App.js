import { Route,Routes } from 'react-router-dom';
import './App.scss';
import IndexPage from './pages/IndexPage/IndexPage';
import ProductsPage from './pages/ProductsPage/ProductsPage';
import ProductPage from './pages/ProductPage/ProductPage';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path={'/'} element={<IndexPage/>}></Route>
        <Route path={'/products'}>
            <Route index element={<ProductsPage/>}></Route>
            <Route path={':id'} element={<ProductPage/>}></Route>
        </Route>
      </Routes>  
    </div>  
  );
}

export default App;
